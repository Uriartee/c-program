#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){

    const int longitud = 6;

    int calculo = 0;

    if (longitud%2 == 0)
        calculo = longitud/2;
    if (longitud%2 == 1){
        
        calculo = longitud/2;
        calculo = calculo + 1;
    }
    
    for (int i = 0; i < longitud; i++){

        if (i%2 == 0){

            for (int z = 0; z < longitud; z++){

                for (int j = 0; j < calculo; j++){
                    for (int x = 0; x < longitud; x++)
                        printf(" *");
                    for (int y = 0; y < longitud; y++)
                        printf(" .");
                }

                printf("\n");

            }
            

        }
        if (i%2 == 1){

            for (int z = 0; z < longitud; z++){

                for (int j = 0; j < calculo; j++){
                    for (int x = 0; x < longitud; x++)
                        printf(" .");
                    for (int y = 0; y < longitud; y++)
                        printf(" *");
                }

                printf("\n");

            }

        }
        
    }

    return EXIT_SUCCESS;

}