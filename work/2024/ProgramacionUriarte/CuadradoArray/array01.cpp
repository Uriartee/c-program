#include <stdio.h>
#include <stdlib.h>

#define N 5

int main(int argc, char *argv[]){

    unsigned A[N][N];

    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++)
            A[i][j] = 1;

    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++)
            printf("%u ", A[i][j]);
        printf("\n");
    }

    return EXIT_SUCCESS;

}