#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

//-----------------------
//Cargar cosas en memoria
//-----------------------

int main (int argc, char *argv[]){

    char hexa[MAX];

    printf("Mete un dato: ");
    scanf("%[0-9a-fA-F]", hexa);
    printf("%s \n", hexa);

    return EXIT_SUCCESS;

}