#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

//-----------------------
//Cargar cosas en memoria
//-----------------------

int main (int argc, char *argv[]){

    char texto[MAX];

    printf("Mete un nombre: ");
    scanf("%s", texto);
    printf("Que tal estas ");
    printf("%s", texto);
    printf("\n");

    for (int i = 0; i < sizeof("%s", texto); i++){

        printf("%i", i);

    }

    return EXIT_SUCCESS;

}