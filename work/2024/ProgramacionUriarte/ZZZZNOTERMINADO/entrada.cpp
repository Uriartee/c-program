#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

//-----------------------
//Cargar cosas en memoria
//-----------------------

int main (int argc, char *argv[]){

    char bytes[MAX];
    char un_byte;

    int entero;
    unsigned sinsigno;
    long int largo;
    unsigned long int ess; //entero sin signo

    int aent[MAX];

    double real;

    printf("Mete un %s: ", "caracter");
    scanf(" %c", &un_byte);
    printf("El %s contiene: %i\n", "caracter", (int) un_byte);

    return EXIT_SUCCESS;

}