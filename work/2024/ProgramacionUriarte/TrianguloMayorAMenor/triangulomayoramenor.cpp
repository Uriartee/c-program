#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){

    const int longitud = 5;
    int contador = 0;

        for (int j=longitud; j>0; j--){

            contador = j;
            while (contador > 0){

                printf("* ");
                contador = contador - 1;

            }

            printf("\n");
            
        }

    return EXIT_SUCCESS;

}