#include <stdio.h>
#include <stdlib.h>

#define N 5

int main(int argc, char *argv[]){

    int x = 0;
    int z = 0;
    int total = 0;
    int contador = 0;

    unsigned A[N][N];

    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++)
            A[i][j] = 1;


    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++){

            if (i==0){

                while (contador < 1){

                    A[i][j] = 1;
                    printf("%5u ", A[i][j]);
                    for (int x=1; x<N; x++){
                        A[i][j] = 0;
                        printf("%5u ", A[i][j]);
                    }
                contador = contador + 1;
                

                }
                
             
            }
            
            if (i > 0){

                if (i-1 == 0 && j-1 == 0){

                    A[i-1][j-1] = 1;

                }
                if (i-1 == 0 && j-1!=0){

                    A[i-1][j-1] = 0;

                }
                if (i-1 == 0 && j!=0){

                    A[i-1][j] = 0;

                }
                if (j-1 < 0){

                    A[i-1][j-1] = 0;

                }
                if (j==0){

                    A[i-1][j] = 1;

                }
                total = A[i-1][j-1] + A[i-1][j];

                A[i][j] = total;
                    
                printf("%5i ", total);

            }
            
        }
        printf("\n\n");
    }

    return EXIT_SUCCESS;

}