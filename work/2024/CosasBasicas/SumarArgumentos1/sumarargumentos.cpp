#include <stdio.h>
#include <stdlib.h>

void usage(FILE *where,const char *error_mssg) {

    fprintf (where, "ERROR: %s !!!!\n", error_mssg);

    if ( where == stderr )
        exit (1);

}

int main(int argc, char *argv[]) {

    int resultado = 0;
    int op1;
    int op2;
 
    if (argc < 3) //argc sirve para ver cuantos argumentos se han metido
        usage(stdout, "Se necesita dos argumento.");

    printf ("N. Argumentos = %i\n", argc);
    //printf ("%s\n", argv[1]); //imprimir el primer agumento
    //printf ("%s\n", argv[2]); //imprimir el segundo agumento

    for (int i = 1; i<argc; i++){
    
    resultado += atoi(argv[i]); //va sumando todos los argumentos

    }

    op1 = atoi(argv[1]); //recoge el argumento 1
    op2 = atoi(argv[2]); //recoge el argumento 2

    //printf ("%i\n", op1);
    //printf ("%i\n", op2);
    printf ("%i\n", resultado);

    return EXIT_SUCCESS;

}