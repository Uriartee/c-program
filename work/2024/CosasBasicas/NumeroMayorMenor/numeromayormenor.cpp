#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main(){

    int numero1;
    cout << "Introduce un numero: ";
    cin >> numero1;
    cout << "----------------------------------------\n";
    
    if (numero1 >= 10){
    
        cout << "Tu numero es mayor o igual que 10\n";
        cout << "Tu numero es: " << numero1 << "\n";
    
    }
    if (numero1 < 10){
    
        cout << "Tu numero es menor que 10\n";
        cout << "Tu numero es: " << numero1 << "\n";
    
    }

    return EXIT_SUCCESS;

}
